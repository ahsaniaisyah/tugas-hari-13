<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AuthController extends Controller
{
    public function form(){
        return view('auth.register');
    }

    public function submit(Request $request){
        $name1 = $request['namadepan'];
        $name2 = $request['namabelakang'];
        return view ('auth.welcome', compact('name1', 'name2'));
    }

}